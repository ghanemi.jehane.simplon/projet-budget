CREATE TABLE operations(id INTEGER PRIMARY KEY AUTO_INCREMENT,
montant DOUBLE NOT NULL,
categorie VARCHAR(256),
date DATE
 );

 INSERT INTO operations(montant, categorie, date)
 VALUES (100, 'Alimentaire', '2021-06-22');

 INSERT INTO operations(montant, categorie,date)
 VALUES (22, 'Loisirs', '2021-06-19'), (50, 'Abonnements', '2021-06-05'), (500, 'Loyer', '2021-06-10');

 INSERT INTO operations(montant, categorie, date)
 VALUES (100, 'Transport', '2021-06-07'),
 (80, 'Assurance', '2021-06-08'),
 (300, 'Vacances', '2021-06-09');