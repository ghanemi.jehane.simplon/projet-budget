import { connection } from './connection';
import { Operation } from '../entity/Operation';

//Méthode findOperationById

export async function findOperationById(id){
    const [rows] = await connection.execute('SELECT * FROM operations WHERE id=?', [id]);
    if (rows.length === 1) {
        return new Operation(rows[0].montant, rows[0].categorie, rows[0].date, rows[0].id)
    }
    return null;
}

//Méthode findAllOperation

export async function findAllOperations(){
    const [rows] = await connection.execute('SELECT * FROM operations');

    const operations = [];
    for(const row of rows){
        let instance = new Operation(row.montant, row.categorie, row.date, row.id);
        operations.push(instance);
    }
    return operations;
}

//Méthode addOperation

export async function addOperation(operation){
   const [rows] = await connection.execute('INSERT INTO operations (montant, categorie, date) VALUES (?,?,?)', [operation.montant, operation.categorie, operation.date]);
   operation.id= rows.insertId; 

}


//Méthode deleteOperation

export async function deleteOperation(id){
    const [rows] = await connection.execute('DELETE FROM operations WHERE id=?', [id]);
}

//Méthode updateOperation

export async function updateOperation(operation){
    const[rows] = await connection.query('UPDATE operations SET montant=?, categorie=?, date=? WHERE id=?', [operation.montant, operation.categorie, operation.date, operation.id])
}

//Méthode findByCategory

export async function findByCategory(categorie){

    const [rows] = await connection.execute('SELECT * FROM operations WHERE categorie=?', [categorie]);

    const operations = [];
    for(const row of rows){
        let instance = new Operation(row.montant, row.categorie, row.date, row.id);
        operations.push(instance);
    }
    return operations;

    

}

// 

// Méthode findByMonth

export async function findByDate(date){
    const [rows] = await connection.execute('SELECT * FROM operations WHERE MONTH(date)=?', [date]);

    const operations = [];
    for(const row of rows){
        let instance = new Operation(row.montant, row.categorie, row.date, row.id);
        operations.push(instance);
    }
    return operations;
}



