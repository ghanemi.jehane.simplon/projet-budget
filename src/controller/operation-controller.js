import {Router} from "express";
import { addOperation, deleteOperation, findAllOperations, findByCategory, findByDate, findOperationById, updateOperation } from "../repository/operationRepository";

export const operationController = Router();

//Méthode findOperationById
operationController.get('/:id', async (req, resp) =>{
    let operation = await findOperationById(req.params.id)
    if(!operation){
        resp.status(404).json({error: 'Not found'});
        return;
    }
    
    resp.json(operation)
})

// Méthode findAllOperations
operationController.get('/', async (req, resp) => {
let operation = await findAllOperations();
resp.json(operation);
});


// Méthode addOperation
operationController.post('/', async(req, resp) =>{
await addOperation(req.body);
resp.json(req.body);
})
//Méthode deleteOperation

operationController.delete('/:id',async (req,resp)=>{
    try{
        await deleteOperation(req.params.id);
        resp.status(204).end()
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }

})

//Méthode updateOperation

operationController.patch('/:id', async (req, resp) =>{
    await updateOperation(req.body);
    resp.end();
})

//Méthode findByCategorie

operationController.get('/operations/:categorie', async(req, resp)=>{
    let operation = await findByCategory(req.params.categorie)

    
    resp.json(operation)

})

//Méthode findByMonth
operationController.get('/operations/month/:date', async(req, resp)=>{
    let operation = await findByDate(req.params.date)

    resp.json(operation)

})