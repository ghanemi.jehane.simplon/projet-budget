export class Operation{
    id;
    montant;
    categorie;
    date;

    /**
     * 
     * @param {Number} montant 
     * @param {String} categorie 
     * @param {Date} date 
     * @param {Number} id 
     */
    constructor(montant, categorie, date, id){
        this.montant = montant;
        this.categorie = categorie;
        this.date = date;
        this.id = id;
    }
}