import express from 'express';
import { operationController } from "./controller/operation-controller";
import cors from "cors";

export const server = express();

server.use(express.json());
server.use(cors())

server.use('/api/budget', operationController)