import { server } from "../src/server";
import request from "supertest";

describe('Test of Operation root,', ()=>{
it('should return operation by id ', async ()=>{
    let response = await request(server)
    .get('/api/budget/1')
    .expect(200);

    expect(response.body).toEqual({
        id: 1,
        montant: expect.any(Number),
        categorie: expect.any(String),
        date: expect.any(String)



    })
})

it("It should return a table of operations in GET", async()=>{
    let response = await request(server)
            .get('/api/budget/')
            .expect(200);
          //response have to be the table of the all users in the database
            expect(response.body).toContainEqual({
                id: expect.any(Number),
                montant: expect.any(Number),
                categorie: expect.any(String),
                date: expect.any(String)
            });
    })


//     it('It should return a table of operations by month', async()=>{
//         let response = await request(server)
//         .get('/api/budget/operationsmonth/:date')
//         .expect(200);
//         expect(response.body).toEqual({
//             id: expect.any(Number),
//             montant: expect.any(Number),
//             categorie: expect.any(String),
//             date: expect.any(String)
        
//     })
// });

});